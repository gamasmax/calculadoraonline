/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadoratemperatura;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

/**
 *
 * @author Aluno
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Text kelvin;
    @FXML
    private Text celsius;
    @FXML
    private Text fahrenheit;
    @FXML
    private TextField temperatura;
    @FXML
    private void getCelsius(ActionEvent event) {
        celsius.setText(temperatura.getText());
        double fahr = (Double.parseDouble(temperatura.getText())*1.80)+32;
        fahrenheit.setText(Double.toString(fahr));
        double kel = Double.parseDouble(temperatura.getText())+273.15;
        kelvin.setText(Double.toString(kel));
    }
    
    @FXML
    private void getKelvin (ActionEvent event){
        kelvin.setText(temperatura.getText());
        double fahr = ((Double.parseDouble(temperatura.getText())-273.15)*1.80)+32;
        fahrenheit.setText(Double.toString(fahr));
        double cel = Double.parseDouble(temperatura.getText())-273.15;
        celsius.setText(Double.toString(cel));
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
